<?php

/*
 * Implements hook_preprocess_page().
 *
 * Add one-off path-based template suggestions.
 * 
 **/
function enigmatic_preprocess_page(&$variables) {
  $path = drupal_get_path_alias();
  if ($path == 'mysubsitefront') {
    $variables['theme_hook_suggestions'][] = 'page__mysubsite';
  }  
}